/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ecmascript/js_shared_map_iterator.h"

#include "ecmascript/builtins/builtins_errors.h"
#include "ecmascript/element_accessor-inl.h"
#include "ecmascript/js_array.h"
#include "ecmascript/js_shared_map.h"
#include "ecmascript/linked_hash_table.h"
#include "ecmascript/object_factory.h"

namespace panda::ecmascript {
using BuiltinsBase = base::BuiltinsBase;
JSTaggedValue JSSharedMapIterator::Next(EcmaRuntimeCallInfo *argv)
{
    ASSERT(argv);
    JSThread *thread = argv->GetThread();
    [[maybe_unused]] EcmaHandleScope handleScope(thread);
    // 1.Let O be the this value
    JSHandle<JSTaggedValue> thisObj(BuiltinsBase::GetThis(argv));
    return NextInternal(thread, thisObj);
}

JSTaggedValue JSSharedMapIterator::NextInternal(JSThread *thread, JSHandle<JSTaggedValue> thisObj)
{
    // 3.If O does not have all of the internal slots of a Map Iterator Instance (23.1.5.3), throw a TypeError
    // exception.
    if (!thisObj->IsJSSharedMapIterator()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "this value is not a map iterator", JSTaggedValue::Exception());
    }
    JSHandle<JSSharedMapIterator> iter(thisObj);
    JSHandle<JSSharedMap> iteratedMap(thread, iter->GetIteratedMap());
    if (iter->GetExpectedModCount() != iteratedMap->GetModCount()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Concurrent modification during iteration", JSTaggedValue::Exception());
    }
    JSHandle<JSTaggedValue> undefinedHandle(thread, JSTaggedValue::Undefined());
    // 4.Let m be O.[[IteratedMap]].
    JSHandle<LinkedHashMap> map(thread, iteratedMap->GetLinkedMap());
    // 5.Let index be O.[[MapNextIndex]].
    int index = static_cast<int>(iter->GetNextIndex());
    IterationKind itemKind = iter->GetIterationKind();
    // 7.If m is undefined, return CreateIterResultObject(undefined, true).
    if (map.GetTaggedValue().IsUndefined()) {
        return JSIterator::CreateIterResultObject(thread, undefinedHandle, true).GetTaggedValue();
    };

    int totalElements = map->NumberOfElements() + map->NumberOfDeletedElements();

    JSMutableHandle<JSTaggedValue> keyHandle(thread, JSTaggedValue::Undefined());
    while (index < totalElements) {
        JSTaggedValue key = map->GetKey(index);
        if (!key.IsHole()) {
            iter->SetNextIndex(index + 1);
            keyHandle.Update(key);
            // If itemKind is key, let result be e.[[Key]]
            if (itemKind == IterationKind::KEY) {
                return JSIterator::CreateIterResultObject(thread, keyHandle, false).GetTaggedValue();
            }
            JSHandle<JSTaggedValue> value(thread, map->GetValue(index));
            // Else if itemKind is value, let result be e.[[Value]].
            if (itemKind == IterationKind::VALUE) {
                return JSIterator::CreateIterResultObject(thread, value, false).GetTaggedValue();
            }
            // Else
            ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
            JSHandle<TaggedArray> array(factory->NewTaggedArray(2));  // 2 means the length of array
            array->Set(thread, 0, keyHandle);
            array->Set(thread, 1, value);
            JSHandle<JSTaggedValue> keyAndValue(JSArray::CreateArrayFromList(thread, array));
            return JSIterator::CreateIterResultObject(thread, keyAndValue, false).GetTaggedValue();
        }
        index++;
    }
    // 13.Set O.[[IteratedMap]] to undefined.
    iter->SetIteratedMap(thread, JSTaggedValue::Undefined());
    return JSIterator::CreateIterResultObject(thread, undefinedHandle, true).GetTaggedValue();
}

JSHandle<JSTaggedValue> JSSharedMapIterator::CreateMapIterator(JSThread *thread, const JSHandle<JSTaggedValue> &obj,
                                                         IterationKind kind)
{
    ObjectFactory *factory = thread->GetEcmaVM()->GetFactory();
    if (!obj->IsJSSharedMap()) {
        JSHandle<JSTaggedValue> undefinedHandle(thread, JSTaggedValue::Undefined());
        THROW_TYPE_ERROR_AND_RETURN(thread, "obj is not SharedMap", undefinedHandle);
    }
    JSHandle<JSTaggedValue> iter(factory->NewJSMapIterator(JSHandle<JSSharedMap>(obj), kind));
    return iter;
}
}  // namespace panda::ecmascript
