/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ecmascript/js_shared_map.h"

#include "ecmascript/js_tagged_value.h"
#include "ecmascript/linked_hash_table.h"
#include "ecmascript/object_factory.h"

namespace panda::ecmascript {
void JSSharedMap::Set(JSThread *thread, const JSHandle<JSSharedMap> &map, const JSHandle<JSTaggedValue> &key,
                const JSHandle<JSTaggedValue> &value)
{
    if (!LinkedHashMap::IsKey(key.GetTaggedValue())) {
        THROW_TYPE_ERROR(thread, "the value must be Key of JSSet");
    }
    JSHandle<LinkedHashMap> mapHandle(thread, LinkedHashMap::Cast(map->GetLinkedMap().GetTaggedObject()));

    uint32_t oldModCount = map->GetModCount();
    JSHandle<LinkedHashMap> newMap = LinkedHashMap::Set(thread, mapHandle, key, value);
    map->SetLinkedMap(thread, newMap);
    auto result = Barriers::AtomicSetPrimitive(map.GetTaggedValue().GetTaggedObject(),
        JSSharedMap::MOD_COUNT_OFFET, oldModCount, oldModCount + 1);
    if (result != oldModCount) {
        THROW_TYPE_ERROR(thread, "Concurrent modification exception");
    }
}

bool JSSharedMap::Delete(JSThread *thread, const JSHandle<JSSharedMap> &map, const JSHandle<JSTaggedValue> &key)
{
    JSHandle<LinkedHashMap> mapHandle(thread, LinkedHashMap::Cast(map->GetLinkedMap().GetTaggedObject()));
    int entry = mapHandle->FindElement(thread, key.GetTaggedValue());
    if (entry == -1) {
        return false;
    }
    uint32_t oldModCount = map->GetModCount();
    mapHandle->RemoveEntry(thread, entry);
    auto result = Barriers::AtomicSetPrimitive(map.GetTaggedValue().GetTaggedObject(),
        JSSharedMap::MOD_COUNT_OFFET, oldModCount, oldModCount + 1);
    if (result != oldModCount) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Concurrent modification exception", false);
    }
    return true;
}

void JSSharedMap::Clear(JSThread *thread, const JSHandle<JSSharedMap> &map)
{
    uint32_t oldModCount = map->GetModCount();
    JSHandle<LinkedHashMap> mapHandle(thread, LinkedHashMap::Cast(map->GetLinkedMap().GetTaggedObject()));
    JSHandle<LinkedHashMap> newMap = LinkedHashMap::Clear(thread, mapHandle);
    map->SetLinkedMap(thread, newMap);
    auto result = Barriers::AtomicSetPrimitive(map.GetTaggedValue().GetTaggedObject(),
        JSSharedMap::MOD_COUNT_OFFET, oldModCount, oldModCount + 1);
    if (result != oldModCount) {
        THROW_TYPE_ERROR(thread, "Concurrent modification exception");
    }
}

bool JSSharedMap::Has(JSThread *thread, JSTaggedValue key) const
{
    uint32_t oldModCount = GetModCount();
    auto result = LinkedHashMap::Cast(GetLinkedMap().GetTaggedObject())->Has(thread, key);
    if (oldModCount != GetModCount()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Concurrent modification exception", false);
    }
    return result;
}

JSTaggedValue JSSharedMap::Get(JSThread *thread, JSTaggedValue key) const
{
    uint32_t oldModCount = GetModCount();
    auto result = LinkedHashMap::Cast(GetLinkedMap().GetTaggedObject())->Get(thread, key);
    if (oldModCount != GetModCount()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Concurrent modification exception", JSTaggedValue::Undefined());
    }
    return result;
}

uint32_t JSSharedMap::GetSize(JSThread *thread) const
{
    uint32_t oldModCount = GetModCount();
    auto result = LinkedHashMap::Cast(GetLinkedMap().GetTaggedObject())->NumberOfElements();
    if (oldModCount != GetModCount()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Concurrent modification exception", 0);
    }
    return result;
}

JSTaggedValue JSSharedMap::GetKey(JSThread *thread, uint32_t entry) const
{
    ASSERT_PRINT(entry >= 0 && entry < GetSize(thread), "entry must be non-negative integer less than capacity");
    uint32_t oldModCount = GetModCount();
    auto result = LinkedHashMap::Cast(GetLinkedMap().GetTaggedObject())->GetKey(entry);
    if (oldModCount != GetModCount()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Concurrent modification exception", JSTaggedValue::Undefined());
    }
    return result;
}

JSTaggedValue JSSharedMap::GetValue(JSThread *thread, uint32_t entry) const
{
    ASSERT_PRINT(entry >= 0 && entry < GetSize(thread), "entry must be non-negative integer less than capacity");
    uint32_t oldModCount = GetModCount();
    auto result = LinkedHashMap::Cast(GetLinkedMap().GetTaggedObject())->GetValue(entry);
    if (oldModCount != GetModCount()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Concurrent modification exception", JSTaggedValue::Undefined());
    }
    return result;
}
}  // namespace panda::ecmascript
