/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ecmascript/js_shared_set.h"

#include "ecmascript/js_tagged_value.h"
#include "ecmascript/linked_hash_table.h"
#include "ecmascript/object_factory.h"

namespace panda::ecmascript {
void JSSharedSet::Add(JSThread *thread, const JSHandle<JSSharedSet> &set, const JSHandle<JSTaggedValue> &value)
{
    if (!LinkedHashSet::IsKey(value.GetTaggedValue())) {
        THROW_TYPE_ERROR(thread, "the value must be Key of JSSharedSet");
    }
    JSHandle<LinkedHashSet> setHandle(thread, LinkedHashSet::Cast(set->GetLinkedSet().GetTaggedObject()));
    uint32_t oldModCount = set->GetModCount();
    JSHandle<LinkedHashSet> newSet = LinkedHashSet::Add(thread, setHandle, value);
    set->SetLinkedSet(thread, newSet);
    auto result = Barriers::AtomicSetPrimitive(set.GetTaggedValue().GetTaggedObject(),
        JSSharedSet::MOD_COUNT_OFFET, oldModCount, oldModCount + 1);
    if (result != oldModCount) {
        THROW_TYPE_ERROR(thread, "Concurrent modification exception");
    }
}

bool JSSharedSet::Delete(JSThread *thread, const JSHandle<JSSharedSet> &set, const JSHandle<JSTaggedValue> &value)
{
    JSHandle<LinkedHashSet> setHandle(thread, LinkedHashSet::Cast(set->GetLinkedSet().GetTaggedObject()));
    int entry = setHandle->FindElement(thread, value.GetTaggedValue());
    if (entry == -1) {
        return false;
    }
    uint32_t oldModCount = set->GetModCount();
    setHandle->RemoveEntry(thread, entry);
    auto result = Barriers::AtomicSetPrimitive(set.GetTaggedValue().GetTaggedObject(),
        JSSharedSet::MOD_COUNT_OFFET, oldModCount, oldModCount + 1);
    if (result != oldModCount) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Concurrent modification exception", false);
    }
    return true;
}

void JSSharedSet::Clear(JSThread *thread, const JSHandle<JSSharedSet> &set)
{
    uint32_t oldModCount = set->GetModCount();
    JSHandle<LinkedHashSet> setHandle(thread, LinkedHashSet::Cast(set->GetLinkedSet().GetTaggedObject()));
    JSHandle<LinkedHashSet> newSet = LinkedHashSet::Clear(thread, setHandle);
    set->SetLinkedSet(thread, newSet);
    auto result = Barriers::AtomicSetPrimitive(set.GetTaggedValue().GetTaggedObject(),
        JSSharedSet::MOD_COUNT_OFFET, oldModCount, oldModCount + 1);
    if (result != oldModCount) {
        THROW_TYPE_ERROR(thread, "Concurrent modification exception");
    }
}

bool JSSharedSet::Has(JSThread *thread, JSTaggedValue value) const
{
    uint32_t oldModCount = GetModCount();
    auto result = LinkedHashSet::Cast(GetLinkedSet().GetTaggedObject())->Has(thread, value);
    if (oldModCount != GetModCount()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Concurrent modification exception", false);
    }
    return result;
}

uint32_t JSSharedSet::GetSize(JSThread *thread) const
{
    uint32_t oldModCount = GetModCount();
    auto result = LinkedHashSet::Cast(GetLinkedSet().GetTaggedObject())->NumberOfElements();
    if (oldModCount != GetModCount()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Concurrent modification exception", 0);
    }
    return result;
}

JSTaggedValue JSSharedSet::GetValue(JSThread *thread, int entry) const
{
    ASSERT_PRINT(entry >= 0 && static_cast<uint32_t>(entry) < GetSize(thread),
        "entry must be non-negative integer less than capacity");
    uint32_t oldModCount = GetModCount();
    auto result = LinkedHashSet::Cast(GetLinkedSet().GetTaggedObject())->GetValue(entry);
    if (oldModCount != GetModCount()) {
        THROW_TYPE_ERROR_AND_RETURN(thread, "Concurrent modification exception", JSTaggedValue::Undefined());
    }
    return result;
}
}  // namespace panda::ecmascript
